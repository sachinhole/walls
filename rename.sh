#!/bin/bash

 i=1

 rpng(){
  for fi in *.png; do
       a="$(printf '%03d' "$i")_wall.png"
       [[ -e $a ]] || mv "$fi" "$a"
       i=$((i+1))
  done
}

  rjpg(){
    for fi in *.jpg; do
      a="$(printf '%03d' "$i")_wall.jpg"
      [[ -e $a ]] || mv "$fi" "$a"
      i=$((i+1))
    done
  }

# rename
rpng
rjpg
